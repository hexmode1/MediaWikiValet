<?php

require_once __DIR__ . '/vendor/autoload.php';

use Dotenv\Dotenv;

class MediaWikiValetDriver extends BasicValetDriver
{
    private $env = null;

    protected function getEnv( string $key ): ?string
    {
        return $_ENV[$key] ?? null;
    }

    /**
     * Determine if the driver serves the request.
     *
     * @param  string $sitePath
     * @param  string $siteName
     * @param  string $uri
     * @return bool
     */
    public function serves( $sitePath, $siteName, $uri )
    {
        // This is valet. We aren't worried about threads and getenv is safe here.
        $this->env = Dotenv::createUnsafeMutable($sitePath);

        try {
            $this->env->load();
            $this->env->required('WEBROOT')->notEmpty();
            $this->env->required('MW_INSTALL_PATH')->notEmpty();

            if (!is_dir($sitePath . "/" . $this->getEnv('WEBROOT')) ) {
                throw new Exception(
                    "'WEBROOT' should be a relative path to directory"
                );
            }
        } catch ( Exception $e ) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    public function isStaticFile( $sitePath, $siteName, $uri )
    {
        $sitePath .= "/" . $this->getEnv('WEBROOT');
        $uriPath = $sitePath . $uri;

        $extension = strtolower(pathinfo($uri, PATHINFO_EXTENSION));

        // Files should be accessible
        if (is_readable($uriPath) && is_file($uriPath) && $extension !== "php" && $extension !== "" ) {
            return $uriPath;
        }
        return false;
    }

    /**
     * Get the fully resolved path to the application's front controller.
     *
     * @param  string $sitePath
     * @param  string $siteName
     * @param  string $uri
     * @return string
     */
    public function frontControllerPath($sitePath, $siteName, $uri = 'index.php')
    {
        // add the public folder to the sitePath
        $sitePath .= "/" . $this->getEnv('WEBROOT');
        $uriPath = $sitePath . $uri;

        // Files should be accessible
        if (is_readable($uriPath) && is_file($uriPath) ) {
            return realpath($uriPath);
        }

        // smart handle the requests, defaulting to index.php in directories and hiding php extensions
        $check = [ "/index.php", "/index.html" ];
        foreach ( $check as $page ) {
            $checkPage = realpath($uriPath . $page);

            if (is_readable($checkPage) ) {
                return $checkPage;
            }
        }

        $mwPath = realpath($this->getEnv('MW_INSTALL_PATH'));
        if ($mwPath === false ) {
            echo "Please set MW_INSTALL_PATH";
            exit;
        }

        $part = explode("/", trim($uri, "/"));
        $tmpPath = $mwPath;
        $mwPath = "$mwPath/index.php";
        foreach ( $part as $p ) {
            $tmpPath .= "/$p";
            if (is_dir($tmpPath) ) {
                $mwPath = "$tmpPath/index.php";
                continue;
            }
            if (is_file($tmpPath) ) {
                $mwPath = $tmpPath;
                break;
            }
        }

        return $mwPath;
    }
}
