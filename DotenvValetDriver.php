<?php
/**
 * A valet driver for PHP applications with a dot-env file
 *
 * PHP version 7+
 *
 * Copyright (C) 2023  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category Driver
 * @package  Valet
 * @author   Mark A. Hershberger <mah@everybody.org>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL3
 * @version  GIT: 0.0.1
 * @link     https://wiki.nichework.com/
 */

require_once 'vendor/autoload.php';

use Dotenv\Dotenv;

class DotenvValetDriver extends ValetDriver
{
    private $_env = null;

    /**
     * Internal helper function to get an environment variable
     */
    protected function getEnv( string $key ): ?string
    {
        return $_ENV[$key] ?? null;
    }

    /**
     * Determine if the driver serves the request.
     *
     * @param  string $sitePath
     * @param  string $siteName
     * @param  string $uri
     * @return bool
     */
    public function serves($sitePath, $siteName, $uri)
    {
        $this->_env = Dotenv::createImmutable($sitePath);

        try {
            $this->_env->load();
            $this->_env->required('WEBROOT')->notEmpty();

            // This allows us to use the .env file with our MediaWiki driver which
            // sets this path (and is not always the same as WEBROOT).
            if (!empty($this->getEnv('MW_INSTALL_PATH'))) {
                throw new Exception("'MW_INSTALL_PATH' is set");
            }

            if (!is_dir($sitePath . "/" . $this->getEnv('WEBROOT')) ) {
                throw new Exception(
                    "'WEBROOT' should be a relative path to directory"
                );
            }
        } catch ( Exception $e ) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the incoming request is for a static file.
     *
     * @param  string $sitePath
     * @param  string $siteName
     * @param  string $uri
     * @return string|false
     */
    public function isStaticFile($sitePath, $siteName, $uri)
    {
        return realpath($sitePath . '/' . $uri);
    }

    /**
     * Get the fully resolved path to the application's front controller.
     *
     * @param  string $sitePath
     * @param  string $siteName
     * @param  string $uri
     * @return string
     */
    public function frontControllerPath($sitePath, $siteName, $uri)
    {
        $sitePath .= "/" . $this->getEnv('WEBROOT');
        return $sitePath . '/index.php';
    }
}
